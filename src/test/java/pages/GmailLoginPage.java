package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class GmailLoginPage {

    @FindBy(css = "input[type=\'email\']")
    private WebElement loginInput;

    @FindBy(xpath = "//div[@class=\'dG5hZc\']//span")
    private WebElement nextButton;

    public GmailLoginPage(WebDriver driver){
        PageFactory.initElements(driver,this);
    }

    public void typeLoginAndSubmit (String login){
        loginInput.sendKeys(login);
        nextButton.click();
    }
}
