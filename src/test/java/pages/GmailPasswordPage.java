package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class GmailPasswordPage {

    @FindBy(css = "input[type=\'password\']")
    private WebElement passwordInput;

    @FindBy(xpath = "//div[@class=\'dG5hZc\']//span")
    private WebElement nextButton;

    public GmailPasswordPage(WebDriver driver){
        PageFactory.initElements(driver,this);
    }

    public void typePasswordAndSubmit (String password){
        passwordInput.sendKeys(password);
        nextButton.click();
    }
}
