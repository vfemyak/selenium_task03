package pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class GmailHomePage {

    @FindBy(xpath = "//div[@class=\'z0\']/descendant::div[@role=\'button\']")
    private WebElement composeButton;

    @FindBy(css = "div.wO textarea.vO")
    private WebElement toTextarea;

    @FindBy(xpath = "//div[@class='oL aDm az9']/span[@email]")
    private WebElement toTextareaCompare;

    @FindBy(xpath = "//input[@name=\'subjectbox\']")
    private WebElement subjectTextarea;

    @FindBy(xpath = "//table[@class=\'iN\']/descendant::*[@role=\'textbox\']")
    private WebElement messageTextarea;

    @FindBy(xpath = "//td[@class=\'Hm\']/img[@class=\'Ha\']")
    private WebElement saveAndCloseButton;

    @FindBy(xpath = "//div[@class=\'TK\']/div[last()]")
    private WebElement draftFolder;

    @FindBy(xpath = "*//div[@role=\'main\']//div[@class=\'xT\']/span[1]")     //select first draft
    private WebElement lastDraftMessage;

    @FindBy(xpath = "//table[@class=\'IZ\']/descendant::*[@role=\'button\']")
    private WebElement sendButton;

    public GmailHomePage(WebDriver driver){
        PageFactory.initElements(driver,this);
    }

    public void clickCompose(){
        composeButton.click();
    }

    public void writeLetterAndClose(String to, String subject, String message) throws InterruptedException {
        toTextarea.sendKeys(to);
        subjectTextarea.sendKeys(subject);
        messageTextarea.sendKeys(message);
        saveAndCloseButton.click();
    }

    public void openDraftFolder(){
        draftFolder.click();
    }

    public void openDraftMessage(String to, String subject, String message) throws InterruptedException {
        lastDraftMessage.click();

        Assert.assertEquals("Destination address is true",to,toTextareaCompare.getAttribute("email"));
        Assert.assertEquals("Subject of letter is true",subject,subjectTextarea.getAttribute("value"));
        Assert.assertEquals("Message of letter is true",message, messageTextarea.getText());
    }
    public void sendDraftMessage(){
        sendButton.click();
    }
}
