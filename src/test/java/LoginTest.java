import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.GmailHomePage;
import pages.GmailLoginPage;
import pages.GmailPasswordPage;

import java.util.concurrent.TimeUnit;

public class LoginTest {

    private WebDriver driver = new ChromeDriver();

    private final String login = "vfemyaktest";
    private final String password = "??????????";
    private final String to = "vfemyak@gmail.com";
    private final String subject = "tessst task3";
    private final String message = "Testtting";


    @BeforeClass
    public static void init(){
        System.setProperty("webdriver.chrome.driver","C:/Users/vfemy/IdeaProjects/chromedriver.exe");
    }

    @Test
    public void gmailLoginTest() throws InterruptedException {
        driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
        driver.get("https://www.google.com/gmail/");

        /*typing login*/
        GmailLoginPage gmailLoginPage = new GmailLoginPage(driver);
        gmailLoginPage.typeLoginAndSubmit(login);
        (new WebDriverWait(driver,10))      //waiting for next page
                .until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("input[type=\'password\']")));

        /*typing password*/
        GmailPasswordPage gmailPasswordPage = new GmailPasswordPage(driver);
        gmailPasswordPage.typePasswordAndSubmit(password);
        (new WebDriverWait(driver,10))      //waiting for home page
                .until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class=\'z0\']/descendant::div[@role=\'button\']")));

        /*working with Gmail home page*/
        GmailHomePage gmailHomePage = new GmailHomePage(driver);
        gmailHomePage.clickCompose();
        (new WebDriverWait(driver,10))      //waiting for opening letter form
                .until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[@class=\'Hm\']/img[@class=\'Ha\']")));
        gmailHomePage.writeLetterAndClose(to,subject,message);
        (new WebDriverWait(driver,10))      //waiting for closing letter form
                .until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//td[@class=\'Hm\']/img[@class=\'Ha\']")));
        gmailHomePage.openDraftFolder();
        (new WebDriverWait(driver,10))      //waiting for opening draft messages
                .until(ExpectedConditions.visibilityOfElementLocated(By.xpath("*//div[@role=\'main\']//div[@class=\'xT\']/span[1]")));
        gmailHomePage.openDraftMessage(to,subject,message); // + checking all fields
        (new WebDriverWait(driver,10))      //waiting for opening letter form
                .until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//table[@class=\'IZ\']/descendant::*[@role=\'button\']")));
        gmailHomePage.sendDraftMessage();

        Thread.sleep(4000);         //using this to see result

        driver.quit();
    }
}
